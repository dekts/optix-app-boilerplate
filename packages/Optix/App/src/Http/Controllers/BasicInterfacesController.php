<?php

declare(strict_types=1);

namespace Optix\App\Http\Controllers;

use Illuminate\Http\Request;
use Optix\App\Event;
use Optix\App\Jobs\ProcessEvent;

class BasicInterfacesController extends Controller
{
    /**
     * Shows a single "It works" page :)
     *
     * @return void
     */
    public function itworks()
    {
        return view('OptixApp::itworks');
    }

    /**
     * Shows a simple canvas placeholder
     *
     * @return void
     */
    public function placeholder()
    {
        return view('OptixApp::placeholder');
    }
}
