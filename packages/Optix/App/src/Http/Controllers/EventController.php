<?php

declare(strict_types=1);

namespace Optix\App\Http\Controllers;

use Illuminate\Http\Request;
use Optix\App\Event;
use Optix\App\Jobs\ProcessEvent;

class EventController extends Controller
{
    public function listen(Request $request) {
    
        $event = Event::create([
            'event' => $request->event,
            'payload' => json_encode($request->payload)
        ]);
        
        ProcessEvent::dispatch($event);
        
        return $event;
    }
}
