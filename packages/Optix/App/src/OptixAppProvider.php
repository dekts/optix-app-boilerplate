<?php

declare(strict_types=1);

namespace Optix\App;

use Illuminate\Support\ServiceProvider;

class OptixAppProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        require __DIR__.'/routes/api.php';

        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->loadViewsFrom(__DIR__.'/resources/views', 'OptixApp');
    }
}
