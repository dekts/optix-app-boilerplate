<?php

declare(strict_types=1);

namespace Optix\App;

use RuntimeException;

class GraphQLClient
{
    protected $endpoint = 'https://api.catalufa.net/graphql';

    /**
     * @param string $query
     * @param array $variables
     * @param string|null $token
     * @return array
     * @throws RuntimeException
     * @see https://graphql.org/learn/serving-over-http/#post-request
     */
    public function query(string $query, array $variables = [], ?string $token = null): array
    {
        $headers = ['Content-Type: application/json'];
        if (!is_null($token)) {
            $headers[] = "access_token: $token";
        }
        $context = stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => $headers,
                'content' => json_encode([
                    'query' => $query,
                    'variables' => $variables,
                ]),
            ]
        ]);

        $data = file_get_contents($this->endpoint, false, $context);
        if ($data === false) {
            $error = error_get_last();
            throw new RuntimeException($error['message'], $error['type']);
        }

        return json_decode($data, true) ?: [];
    }
}
