<?php

declare(strict_types=1);

namespace Optix\App\Http\Controllers;

use Illuminate\Http\Request;
use Optix\App\Event;
use Optix\App\Jobs\ProcessEvent;

class AppSettingsController extends Controller
{
    /**
     * Shows the settings page
     *
     * @return void
     */
    public function settings()
    {
        return view('OptixApp::appsettings');
    }
}
