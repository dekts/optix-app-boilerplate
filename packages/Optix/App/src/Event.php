<?php

declare(strict_types=1);

namespace Optix\App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events_received';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event', 'payload',
    ];
}
