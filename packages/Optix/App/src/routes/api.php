<?php

declare(strict_types=1);

use Optix\App\Http\Controllers\EventController;

Route::prefix('api')->group(function () {
    Route::post('events', [EventController::class, 'listen']);
});
