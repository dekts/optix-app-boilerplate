<!DOCTYPE html>
<html>
    <head>
        <link
            href="https://api.optixapp.com/ui-kit/optix-ui-kit.min.css"
            rel="stylesheet"
        />
        <script
            type="text/javascript"
            src="https://api.optixapp.com/ui-kit/optix-ui-kit.min.js"
        ></script>
        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui"
        />
        <style>
            .application--wrap {
                border: 2px dotted #000;
                box-sizing: border-box;
                font-size: 10px;
            }
            .v-content__wrap {
                padding: 5px;
            }
            code {
                display: block;
            }
            .scrollable {
                overflow: auto;
                max-height: calc(100vh - 15px);
            }
            p {
                margin: 0;
            }
            .v-speed-dial {
                position: absolute;
            }
            .v-btn--floating {
                position: relative;
            }
            .face {
                float: left;
                padding: 5px;
            }
            .face img {
                max-height: 50px;
                max-width: 50px;
            }
        </style>
    </head>

    <body>
        <div id="app" class="optix-show-on-load">
            <v-app>
                <v-content>
                    <div class="fixed" v-if="detail==''">
                        <p>
                            <strong>Canvas placeholder</strong> - Size:
                            {{ width }} x {{ height }}px @ {{ dpr }}dpr - Access
                            token:
                            {{ access_token ? access_token : "Not provided" }}
                        </p>
                        <div v-if="access_token">
                            <div
                                v-if="me.user && me.user.picture && me.user.picture.thumb"
                                class="face"
                            >
                                <img :src="me.user.picture.thumb" />
                            </div>
                            <div v-if="me.authType">
                                AuthType: {{ me.authType }}
                            </div>
                            <div v-if="me.user">User: {{ me.user.name }}</div>
                            <div v-if="me.organization">
                                Organization: {{ me.organization.name }}
                            </div>
                        </div>
                    </div>
                    <div class="scrollable" v-if="detail=='me'">
                        <h4>ME</h4>
                        <code>{{ me }}</code>
                    </div>
                    <div class="scrollable" v-if="detail=='qs'">
                        <h4>Query string</h4>
                        <code>{{ qs }}</code>
                    </div>
                    <div class="scrollable" v-if="detail=='hs'">
                        <h4>Hash string</h4>
                        <code>{{ hs }}</code>
                    </div>
                    <div class="scrollable" v-if="detail=='full'">
                        <h4>URL</h4>
                        <code>{{ fullurl }}</code>
                    </div>

                    <v-speed-dial
                        v-model="fab"
                        :top="true"
                        :right="true"
                        direction="left"
                        :open-on-hover="true"
                        transition="slide-x-transition"
                    >
                        <v-btn
                            slot="activator"
                            v-model="fab"
                            color="blue darken-2"
                            dark
                            fab
                            small
                        >
                            <v-icon>zoom_in</v-icon>
                            <v-icon>close</v-icon>
                        </v-btn>
                        <v-btn
                            fab
                            dark
                            small
                            :color="detail=='full'?'red':'blue darken-2'"
                            @click="detail='full'"
                        >
                            <v-icon>link</v-icon>
                        </v-btn>
                        <v-btn
                            fab
                            dark
                            small
                            :color="detail=='hs'?'red':'blue darken-2'"
                            @click="detail='hs'"
                        >
                            #
                        </v-btn>
                        <v-btn
                            fab
                            dark
                            small
                            :color="detail=='qs'?'red':'blue darken-2'"
                            @click="detail='qs'"
                        >
                            ?
                        </v-btn>
                        <v-btn
                            fab
                            dark
                            small
                            :color="detail=='me'?'red':'blue darken-2'"
                            @click="detail='me'"
                        >
                            <v-icon>accessibility</v-icon>
                        </v-btn>
                        <v-btn
                            fab
                            dark
                            small
                            :color="detail==''?'red':'blue darken-2'"
                            @click="detail=''"
                        >
                            <v-icon>home</v-icon>
                        </v-btn>
                    </v-speed-dial>
                </v-content>
            </v-app>
        </div>

        <script>
            new Vue({
                el: "#app",
                data() {
                    return {
                        // Speed dial
                        fab: false,
                        detail: "", // Detail content
                        // Sizing
                        width: 0,
                        height: 0,
                        dpr: 0,
                        // Query
                        me: {},
                        qs: {},
                        hs: {},
                        fullurl: "",
                        // Token
                        access_token: ""
                    };
                },
                mounted() {
                    this.$optix.init().then(() => {
                        // Screen size
                        let recalcWindow = () => {
                            this.height = window.innerHeight;
                            this.width = window.innerWidth;
                            this.dpr = window.devicePixelRatio;
                        };
                        window.addEventListener("resize", recalcWindow);
                        recalcWindow();

                        // Query string
                        this.fullurl = this.$optix.fullurl;
                        this.qs = this.$optix.qs;
                        this.hs = this.$optix.hs;
                        this.access_token = this.$optix.access_token;

                        // Test request
                        this.$optix
                            .graphQL(
                                ` query me {
                                    me {
                                        authType
                                        user {
                                            name
                                            picture {
                                                    thumb
                                            }
                                        }
                                        organization{
                                            name
                                        }
                                    }
                                }`
                            )
                            .then(response => {
                                this.me = response.data.data.me;
                            });
                    });
                }
            });
        </script>
    </body>
</html>
