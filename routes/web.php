<?php

use Optix\App\Http\Controllers\AppSettingsController;
use Optix\App\Http\Controllers\BasicInterfacesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('placeholder');
});
*/

Route::get('/canvases/app-settings', [AppSettingsController::class, 'settings']);
Route::get('/canvas-placeholder', [BasicInterfacesController::class, 'placeholder']);
Route::get('/', [BasicInterfacesController::class, 'itworks']);
