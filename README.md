# Optix App Boilerplate

This is an example app built using Laravel and Optix UI Kit.

> Some files, like `.env` and `vendor` could available on this repository for startup simplicity, make sure to not commit them when building your app.

## Starting the boilerplate app

Make sure to install PHP and Composer in your dev environment, or use Docker to run the application properly. This project is based on Laravel 5.8.

```
# Install PHP packages
composer install 

# Run PHP Laravel server
php artisan serve

# By default the page http://127.0.0.1:8000/ will be the URL available with a "It works" message!
```

## Endpoints available

* `/`: "It works" sample page.

* `/canvas-placeholder`: Sample canvas placeholder to test canvas placements.

* `/canvases/app-settings`: Sample app settings canvas.

* `/api/events`: Endpoint for webhooks.

## Settings file

Use the following settings file to enable some test canvases:

```
{
  "canvases": [
    {
      "type": "ADMIN_APP_SETTINGS",
      "url": "http://localhost:8000/canvases/app-settings"
    },
    {
      "type": "ADMIN_MAIN_MENU",
      "url": "http://localhost:8000/canvas-placeholder",
      "title": "Test main menu",
      "icon": "file-download"
    }
  ]
}
```

## Accessing the app config interface

To access the app settings of an unpublished app use:

https://YOUR_VENUE.optixapp.com/appSettings?identification=admin_app_settings&client_id=YOUR_CLIENT_ID

> Due to HTTP (non HTTPS url to localhost), make sure to enable the access to insecure resources (look for the shield at right side of URL address field in Google Chrome).

## Database

The boilerplate uses a simple SQLite database. Run the following commands to prepare the environment:

```
touch database/database.sqlite
php artisan migrate
```

> For production environment, use a MySQL database.

## Webhooks

The event queue is stored in the database.
To process this queue, run `php artisan queue:work`
