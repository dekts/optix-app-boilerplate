<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>It works!</title>
</head>
<body>
    <h1>It works</h1>
    <p>This is a sample page based on Optix App Boilerplate.</p>
    <p>You should delete this page for a production version or replace with something more meaningful.</p>
</body>
</html>