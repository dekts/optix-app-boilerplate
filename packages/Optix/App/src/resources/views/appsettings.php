<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link href="https://api.optixapp.com/ui-kit/optix-ui-kit.min.css" rel="stylesheet">
    <script type="text/javascript" src="https://api.optixapp.com/ui-kit/optix-ui-kit.min.js"></script>
    <style>
      /* 
        You can make this CSS external to improve caching, use inline to avoid caching
        Usually native app (iOS/Android) canvases are using agressive caching policies
       */
      
      /* If your application scrolls, you can create a wrapper for it */
      .application--wrap>.v-content {
          height: 100%;
      }
      .application--wrap>.v-content>.v-content__wrap>.main-wrapper {
          height: 100%;
          overflow: scroll;
          padding-bottom: 30px;
          padding-top: 30px;
      }

    </style>
    <body>
    <!-- Class optix-settings-page is a special class that will add the background color to the page based on the settings page of the VM -->
    <div id="app" class="optix-show-on-load optix-settings-page">
      <optix-app>

          <!-- Success message: black, at the bottom -->
          <v-snackbar v-model="success.show" color="black" :timeout="success.timeout" bottom>
              {{success.message}}!
              <v-btn color="white" flat @click="success.show = false">
                  Close
              </v-btn>
          </v-snackbar>

          <!-- Error messages, red, at the top -->
          <v-snackbar v-model="error.show" color="error" :timeout="error.timeout" top multi-line>
              {{error.message}}
              <v-btn dark flat @click="error.show = false">
                  Close
              </v-btn>
          </v-snackbar>

          <div class="main-wrapper">

            <!-- Alignment -->
            <v-layout row wrap justify-center>

              <!-- Create a settings block -->
              <optix-sheet>
                <optix-container vertical>
                  <v-layout row>
                    <v-flex grow>
                      <h1>This is a title</h1>
                      <p>Hello World!</p>
                      <pre>{{ queryResult }}</pre>
                    </v-flex>
                    <v-flex shrink>
                      <v-btn round depressed color="primary" @click="triggerSuccess">
                        Trigger success message
                      </v-btn>
                      <v-btn round depressed color="error" @click="triggerError">
                        Trigger error message
                      </v-btn>
                    </v-flex>
                  </v-layout>
                </optix-container>
              </optix-sheet>
            </v-layout>

            <v-layout row wrap justify-center>

              <!-- Informative block -->
              <optix-sheet label="This block has a label">
                <v-card-text>
                    <h1 class="mt-0">Information</h1>
                    <p>Instruction box. <a target="_blank" href="#">Learn More</a></p>
                </v-card-text>
              </optix-sheet>

            </v-layout>

            <v-layout row wrap justify-center>

              <!-- Fields -->
              <optix-sheet label="This block has a label">
                <optix-container vertical>
                    <h1 class="mt-0">Fields</h1>
                    <v-layout column>
                      <v-flex>
                        <v-text-field 
                          label="Type a number"
                          v-model="numberOfLines"/>
                      </v-flex>
                    </v-layout>
                </optix-container>
              </optix-sheet>

            </v-layout>

            <!-- Dynamic content -->
            <v-layout row wrap justify-center v-if="numberOfLines>1">
              <optix-sheet :label="'Repeating '+numberOfLines+' times'">
                <optix-container vertical>
                  <p v-for="i in parseInt(numberOfLines)" :key="i">Line number is {{i}}</p>
                </optix-container>
              </optix-sheet>
            </v-layout>
            

          </div>

      </optix-app>
    </div>
    <script>
      new Vue({
        el: "#app",
        data: {
          queryResult: {},
          success: {
              message: 'Test Message',
              timeout: 4000,
              show: false
          },
          error: {
              message: 'Test Message',
              timeout: 15000,
              show: false
          },
          numberOfLines: 3,
        },
        methods: {
          triggerError(){
            this.error.message = "This is an error";
            this.error.show = true;
          },
          triggerSuccess(){
            this.success.message = "This is a success";
            this.success.show = true;
          }
        },
        mounted() {
          this.$optix.init().then(() => {
            this.$optix
              .graphQL(
                `query me { 
                me {
                     authType 
                   } 
                }`
              )
              .then(result => {
                console.log(result.data);
                this.queryResult = result.data;
              });
          });
        }
      });
    </script>
  </body>
</html>
